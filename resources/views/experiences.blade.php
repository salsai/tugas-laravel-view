@extends('layout/main')

@section('title', 'Experiences')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Experiences</h1>
        </div>
                @foreach($experiences as $experience)
                  <div>
                    {{ $loop->index }} - {{ $experience['type'] }} - {{ $experience['base'] }}
                    @if($loop->first)
                      <span> </span>
                    @endif
                    @if($loop->last)
                      <span> </span>
                    @endif
                  </div>
                @endforeach
    </div>
</div>
@endsection